import 'package:primenumber/primenumber.dart' as primenumber;
import 'dart:io';

bool lottery(dynamic number) {
  if (number == 123456) {
    return true;
  }
  if (number == 456789) {
    return true;
  }
  if (number == 154789) {
    return true;
  }
  if (number == 225468) {
    return true;
  }
  if (number == 000254) {
    return true;
  }
  if (number == 654725) {
    return true;
  }
  if (number == 358887) {
    return true;
  }
  if (number == 302145) {
    return true;
  }
  if (number == 625000) {
    return true;
  }
  if (number == 654320) {
    return true;
  }
  if (number == 998855) {
    return true;
  }
  if (number == 302459) {
    return true;
  }
  if (number == 774125) {
    return true;
  }
  if (number == 965785) {
    return true;
  }
  return false;
}

void main(List<String> args) {
  print('input your Lottery : ');
  int? yourLottery = int.parse(stdin.readLineSync()!);
  if (lottery(yourLottery)) {
    print('Congratulation!!!');
  } else {
    print('Sorry, you didnt get the award.');
  }
}
